# Skripti mittaustuloksia vastaavien kuvien piirtämiseksi. Käytetty kieli on
# Julia.

# Varmistetaan ympäristön aktiivisuus

import Pkg

println("Varmistetaan, että nykyinen projekti on aktiivinen…")
const YMPÄRISTÖN_KANSIO = normpath(@__DIR__)
if Base.active_project() ≠ YMPÄRISTÖN_KANSIO
	Pkg.activate(YMPÄRISTÖN_KANSIO)
end
println("Varmistetaan riippuvuuksien saatavuus…")
Pkg.instantiate()

# Tuodaan muut riippuvuudet

using Revise
using CairoMakie
using Colors
using LaTeXStrings

# Asetetaaan vakioita

const MASSAKUVAN_NIMI = "massat.pdf"
const PITUUSKUVAN_NIMI = "pituudet.pdf"
const AIKAKUVAN_NIMI = "ajat.pdf"

const MASSKUVAN_NIMI = "masses.pdf"
const LENGTHKUVAN_NIMI = "lengths.pdf"
const TIMEKUVAN_NIMI = "times.pdf"

const MASSAVÄRI = Colors.JULIA_LOGO_COLORS.purple
const PITUUSVÄRI = Colors.JULIA_LOGO_COLORS.green
const AIKAVÄRI = Colors.JULIA_LOGO_COLORS.red

# Määritellään funktiot

"""
Piirtää mittapisteet ja niiden kekiarvon kuvaan ja palauttaa sen.
"""
function pistekeskiarvokuva(
	pystyarvot,
	pystyvirheet,
	keskiarvo,
	karkeat_virheet,
	vaakaotsikko,
	pystyotsikko,
	vaakatikit,
	selitelaatikon_sijainti,
	pistelabel,
	keskiarvolabel,
	virhelabel,
	karkea_virhelabel,
	väri,
)

	println("\nLuodaan kuvaa…")

	kuva = Figure(resolution = (600, 400))

	println("Lisätään kuvaan akselisto…")

	akselit = kuva[1,1] = Axis(
		kuva,
		xlabel=LaTeXString(vaakaotsikko),
		ylabel=LaTeXString(pystyotsikko),
		xticks=vaakatikit,
	)

	println("Lisätään akselistoon virhepalkit…")

	virhepalkit = errorbars!(
		akselit,
		[i for i ∈ 1:length(pystyarvot)],
		pystyarvot,
		pystyvirheet,
		label=LaTeXString(virhelabel),
		color=väri,
		whiskerwidth=10,
	)

	println("Lisätään akselistoon mittapisteet…")

	mittapisteet = scatter!(
		akselit,
		pystyarvot,
		label=LaTeXString(pistelabel),
		color=väri
	)

	karkeat_virheet = if ! isempty(karkeat_virheet)

		println("Lisätään akselistoon karkeat virheet…")

		scatter!(
			akselit,
			karkeat_virheet,
			pystyarvot[karkeat_virheet],
			label=LaTeXString(karkea_virhelabel),
			color=väri,
			marker='×',
			markersize=30,
		)

	end

	println("Lisätään akselistoon keskiarvo…")

	keskiarvoviiva = lines!(
		akselit,
		[i for i ∈ 1:length(pystyarvot)],
		[keskiarvo for _ ∈ 1:length(pystyarvot)],
		label=LaTeXString(keskiarvolabel),
		color=väri,
		linestyle = :dash,
	)

	println("Lisätään vielä selitelaatikko akseliston sisälle…")

	massaselite = axislegend(akselit, position=selitelaatikon_sijainti)

	println("Valmis. Palautetaan kuvaa…")

	kuva

end

"""
Tallentaa annetun kuvan annetulla nimellä PDF-muodossa.
"""
function tallenna_kuva(kuvan_nimi::AbstractString, kuva::CairoMakie.Figure)
	polku = joinpath(YMPÄRISTÖN_KANSIO, "$kuvan_nimi")
	println("Tallennetaan kuvaa $polku…")
	save(polku, kuva)
end

"""
Laskee keskiarvon keskivirheen mittausjonolle.
"""
function keskiarvon_keskivirhe(mittaukset, keskiarvo)
	n = length(mittaukset)
	√(sum((mittaukset .- keskiarvo).^2) / (n * (n-1)))
end

"""
Mittauspöytäkirjan avaimellisena monikkona palauttava vakiofunktio.
"""
function MITTAUSPÖYTÄKIRJA()
	(;
		massat = [52.3, 52.1, 52.1],
		Δmassa = 0.1,
		pituudet = [1.66, 1.67, 1.65],
		Δpituus = 1e-2,
		ajat = [100.02, 98.06, 100.11, 100.03, 100.09],
		ΔTm = 1e-2,
	)
end

"""
Päärutiini. Tätä kutsutaan tiedoston lopussa.
"""
function main()

	# Mittauspöytäkirja palautetaan sen määrittävästä funktiosta

	M = MITTAUSPÖYTÄKIRJA()

	# Lasketaan keskiarvot esimerkin vuoksi

	keskiarvo(vektori) = sum(vektori) / length(vektori)

	massa_ka = keskiarvo(M.massat)
	pituus_ka = keskiarvo(M.pituudet)
	aika_ka = keskiarvo(M.ajat)

	# Ajan mittalaitteen ja tilastollisen virheen yhdistäminen

	ΔTka = keskiarvon_keskivirhe(M.ajat, aika_ka)
	ΔT = √(ΔTka^2 + M.ΔTm^2)

	# Apumuuttujia

	pallorangi = 1:length(M.massat)
	aikarangi = 1:length(M.ajat)

	# Massakuvan piirtäminen tietyillä mittasuhteilla

	massakuva = pistekeskiarvokuva(
		M.massat, # pystyarvot
		[M.Δmassa for _ ∈ 1:length(M.massat)], # pystyvirheet,
		massa_ka, # keskiarvo,
		[], # karkeat_virheet,
		"Pallo", # vaakaotsikko,
		"Massa (kg)", # pystyotsikko,
		pallorangi, # vaakatikit,
		:rt, # selitelaatikon_sijainti,
		"mittaukset", # pistelabel,
		"keskiarvo(mittaukset)", # keskiarvolabel,
		"virherajat(mittaukset)", # virhelabel,
		"karkeat virheet", # karkea_virhelabel
		MASSAVÄRI, # väri,
	)

	tallenna_kuva(MASSAKUVAN_NIMI, massakuva)

	masskuva = pistekeskiarvokuva(
		M.massat, # pystyarvot
		[M.Δmassa for _ ∈ 1:length(M.massat)], # pystyvirheet,
		massa_ka, # keskiarvo,
		[], # karkeat_virheet,
		"Ball", # vaakaotsikko,
		"Mass (kg)", # pystyotsikko,
		pallorangi, # vaakatikit,
		:rt, # selitelaatikon_sijainti,
		"measurements", # pistelabel,
		"mean(measurements)", # keskiarvolabel,
		"error bars(measurements)", # virhelabel,
		"coarse errors", # karkea_virhelabel
		MASSAVÄRI, # väri,
	)

	tallenna_kuva(MASSKUVAN_NIMI, masskuva)

	# Pituuskuvan piirtäminen tietyillä mittasuhteilla

	pituuskuva = pistekeskiarvokuva(
		M.pituudet, # pystyarvot
		[M.Δpituus for _ ∈ 1:length(M.pituudet)], # pystyvirheet
		pituus_ka, # keskiarvo
		[], # karkeat_virheet
		"Pallo", # vaakaotsikko
		"Langan pituus (m)", # pystyotsikko
		pallorangi, # vaakatikit
		:rt, # selitelaatikon_sijainti
		"mittaukset", # pistelabel
		"keskiarvo(mittaukset)", # keskiarvolabel
		"virherajat(mittaukset)", # virhelabel
		"karkeat virheet", # karkea_virhelabel
		PITUUSVÄRI, # väri
	)

	tallenna_kuva(PITUUSKUVAN_NIMI, pituuskuva)

	lengthkuva = pistekeskiarvokuva(
		M.pituudet, # pystyarvot
		[M.Δpituus for _ ∈ 1:length(M.pituudet)], # pystyvirheet
		pituus_ka, # keskiarvo
		[], # karkeat_virheet
		"Ball", # vaakaotsikko
		"String length (m)", # pystyotsikko
		pallorangi, # vaakatikit
		:cb, # selitelaatikon_sijainti
		"measurements", # pistelabel
		"mean(measurements)", # keskiarvolabel
		"error bars(measurements)", # virhelabel
		"coarse errors", # karkea_virhelabel
		PITUUSVÄRI, # väri
	)

	tallenna_kuva(LENGTHKUVAN_NIMI, lengthkuva)

	# Aikakuvan piirtäminen tietyillä mittasuhteilla

	aikakuva = pistekeskiarvokuva(
		M.ajat, # pystyarvot
		[ΔT for _ ∈ 1:length(M.ajat)], # pystyvirheet,
		aika_ka, # keskiarvo,
		[2], # karkeat_virheet,
		"Mittaus", # vaakaotsikko,
		"\$50T\$ (s)", # pystyotsikko,
		aikarangi, # vaakatikit,
		:rb, # selitelaatikon_sijainti,
		"mittaukset", # pistelabel,
		"keskiarvo(mittaukset)", # keskiarvolabel,
		"virherajat(mittaukset)", # virhelabel,
		"karkeat virheet", # karkea_virhelabel
		AIKAVÄRI, # väri,
	)

	tallenna_kuva(AIKAKUVAN_NIMI, aikakuva)

	timekuva = pistekeskiarvokuva(
		M.ajat, # pystyarvot
		[ΔT for _ ∈ 1:length(M.ajat)], # pystyvirheet,
		aika_ka, # keskiarvo,
		[2], # karkeat_virheet,
		"Measurement", # vaakaotsikko,
		"\$50T\$ (s)", # pystyotsikko,
		aikarangi, # vaakatikit,
		:rb, # selitelaatikon_sijainti,
		"measurements", # pistelabel
		"mean(measurements)", # keskiarvolabel
		"error bars(measurements)", # virhelabel
		"coarse errors", # karkea_virhelabel
		AIKAVÄRI, # väri,
	)

	tallenna_kuva(TIMEKUVAN_NIMI, timekuva)

	# Palautetaan päärutiinista tyhjää, jos kaikki meni kuin Strömsössä.

	nothing

end

# Päärutiinin kutsu.

main()
