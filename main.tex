%% main.tex
%
% The report is to be written in this file. Do not forget to add the document
% information to the file docinfo.tex and the sources you need to lahteet.bib.
% You may also add command definitions of your own to the file preamble.tex.

% First load document information.

\input{docinfo.tex}

% Then load the class file, which defines the base funcionality of the report
% template.

\documentclass[english]{tex/tau-fys-oplab-kirjoituspohja}

% Include possible user-defined commands.

\input{preamble.tex}

% The document contents begin.

\begin{document}

% Load title page from the file titlepage.tex

\input{tex/titlepage.tex}

% Create table of contents.

\newpage
\thispagestyle{empty}
\tableofcontents

% The report itself begins…

\newpage
\clearpage
\pagenumbering{arabic}

\section{Introduction}\label{sec:introduction}

The introduction should make it clear to the reader, what quantities will be
discussed in this report, and how they were
measured~\cite{report-writing-instructions}. This way a reader does not have
to read further into the report to find out whether the information they seek
can be found in what they are reading.

\section{Theory}\label{sec:theory}

The purpose of the theory section is to present the theory needed in forming
the results later on in the report. This usually involves formula derivations,
\emph{backed up by citations}, but the connection between the formulae and
their application is also a polite thing to state. A good theory section also
shortly discusses how the quantities being discussed are utilized in practise,
in industry for example.~\cite{report-writing-instructions}

\subsection{Quotient}\label{ssec:quotient}

As an example, this subsection presents the concept of a \emph{quotient}. A
quotient \(q\) can be calculated with the formula
\begin{equation}\label{eq:quotient}
	q
	=
	\frac{a}{b}
	=
	\arcs*{\frac{a}{b}}
\end{equation}
where the quantity \(a\) is the \emph{numerator} and \(b\) the
\emph{denominator} of the quotient \(q\).

A subsection should contain at least two full paragraphs of text. Otherwise
there is too little content to justify the existence of a subsection within a
section.  It is also not worth dividing a section into subsections, unless you
have at least two distinct topics to discuss.

\subsection{Integrals and fonts}\label{ssec:integral}

In subsection~\ref{ssec:quotient} the concept of a quotient was presented in
equation~\eqref{eq:quotient}. According to Trench~\cite[114]{Trench2013}, the
\emph{definite integral} \(I\) of a function \(f : \squares{a,b}\to\Rset\)
over the domain of integration \(\squares{a,b}\) is written as follows:
\begin{equation}\label{eq:integraali}
	I
	=
	\int_{a}^{b} f(x) \diff x
	=
	\squares[\big]{F(x)}_{a}^{b}
	=
	F(b) - F(a)\,,
\end{equation}
if the function \(f\) is integrable. Here \(F\) is the \emph{anti-derivative}
of the function \(f\). Notice the font of the differential operator \(\diff\)
when compared to the fonts used to express the quantity \(x\).

More generally, it should be noted that quantities are to be written in
slanted mathematics font, whereas their units are to be written in an upright
font. For example, the symbol of the quatity \emph{mass} is written as \(m\),
whereas its SI-unit \(\squares{m} = \unit{kg}\). The notation where the symbol
of the quantity is surrounded with square brackets comes from the matrix
formalism of dimensional analysis.~\cite{jyu-dimensioanalyysi}


\section{Performing the experiment}\label{sec:performing the experiment}

In this section the measurements performed in the laboratory are presented in
such a manner that they can be repeated based on the instructions here. To
this end, a set of photographs should be taken of the measuring equipment and
the progression of the measurements, and these should be referenced in the
text by their figure numbers.

To set an example, the measurements were performed using a massive Newton's
cradle~\cite{wiki-cradle}. In Figure~\ref{fig:cradle}, an instance of such a
contraption can be seen.

% The figure environment centers a given image and places the figure caption
% in the right place, below the figure. Called in the form
% \begin{figure}{Caption text.}{reference name} … \end{figure}

\begin{figure}{The measuring equipment used in the experiment.~\cite{img-cradle}}{fig:cradle}

	% An image is included from the folder kuvat/ and its width is set in
	% relation to text width.

	\includegraphics[width=0.4\textwidth]{kuvat/kehto.jpg}

\end{figure}

The measurements were started by deviating an edgemost ball and letting go of
it. After this, \(50\) periods of oscillation were measured, and the durations
of these were written in the log book. This experiment was repeated \(5\)
times. In addition, the lengths of all the ropes the balls were suspended from
were measured with a tape measure and the masses of the balls were recorded
with a scale meant for livestock.


\section{Transcribed logbook}\label{sec:logbook}

In this section, the log book written in the student laboratory is re-written
such that the tables an other observations made during the measurements become
publishable. The tables should be referenced by their numbers and their
contents should be explained in the text, especially if crude erraneous
measurements can be seen in them.~\cite{report-writing-instructions}

For example, the measurements were performed according to the instructions in
section~\ref{sec:performing the experiment}. The constant results can be seen
in Table~\ref{tab:constants}.

% The purpose of this environment is to center a given table and place its
% caption in the correct place, above it.

\begin{table}{ The results of the constant measurements.}{tab:constants}

	% The table enviroment tabular needs to be given how its columns are to be
	% aligned. The letters are l, c and r, correspondingly left, center and
	% right. The string \\ changes rows and the symbol & adds a column. The
	% comman \multicolumn{n of columns}{alignments}{content} creates a cell
	% that spans multiple columns. There is also a similar command \multirow{n
	% of rows}{content width}{contents} for creating cells that span multiple
	% rows.

	\begin{tabular}{c c c}
		\toprule
		Ball & Reading & Accuracy \\
		\midrule
		\multicolumn{3}{c}{masses (kg)} \\
		\midrule
		1 & 52.3 & 0.1 \\
		2 & 52.1 & 0.1 \\
		3 & 52.1 & 0.1 \\
		\midrule
		\multicolumn{3}{c}{string lengths (m)} \\
		\midrule
		1 & 1.66 & 0.01 \\
		2 & 1.67 & 0.01 \\
		3 & 0.65 & 0.01 \\
		\bottomrule
	\end{tabular}

\end{table}

The results in Table~\ref{tab:constants} show that the masses of the balls
deviate from each other at most \(0.2\unit{kg}\), which is more than what the
accuracy of the scale was. In Table~\ref{tab:periods}, the multiples of the
periods \(T\) have been collected.

\begin{taulukko}{Results of the repeated experiment.}{tab:periods}
	\begin{tabular}{c | c c c c c | c}
		\toprule
		Measurement & 1 & 2 & 3 & 4 & 5 & Accuracy \\
		\midrule
		\(50T(\unit{s})\) & 100{,}02 & \color{red} 98{,}06 & 100{,}11 & 100{,}03 & 100{,}09 & 0{,}01 \\
		\bottomrule
	\end{tabular}
\end{taulukko}

There is a crude measurement error in the table, in column 2. The time is
roughly two seconds shorter than the other ones, which implies that during the
second take only \(49\) periods were measured instead of the full \(50\).

\section{Results}\label{sec:results}

In this section, the neede results are formed based on the formulae presented
in section~\ref{sec:theory} and the tables in section~\ref{sec:logbook}. The
formulae and tables are \emph{not} supposed to be repeated here, but instead
their numbers are to be used to reference them, to avoid unnecessary
repetition.~\cite{report-writing-instructions}

As an example, Figures~\ref{fig:masses} and \ref{fig:lengths} were drawn based
on the values in Table~\ref{tab:constants}. The figures also show the averages
of the results, to make it easier to spot any ouliers.

% When there is no empty line between figures, they are placed side by aside,
% as long as their sizes are adjusted to make this possible. The command
% \hfill fills all of the remaining space between the images with whitespace.

\begin{figure}[0.49\textwidth]{Sphere masses.}{fig:masses}
	\includegraphics[width=\textwidth]{kuvat/mittaustulokset/masses.pdf}
\end{figure}
\hfill
\begin{figure}[0.49\textwidth]{Lengths of the strings.}{fig:lengths}
	\includegraphics[width=\textwidth]{kuvat/mittaustulokset/lengths.pdf}
\end{figure}

From the figures it can be seen that the constant measurements were fairly
successful: the deviations from the means are fairly small.
Figure~\ref{fig:times} shows a graphical representation of the period
measurements. The mean of the results is also displayed, for the same reasons
as before in Figures~\ref{fig:masses} and \ref{fig:lengths}.

\begin{kuva}{Results of the period measurements.}{fig:times}
	\includegraphics[width=0.8\textwidth]{kuvat/mittaustulokset/times.pdf}
\end{kuva}

Here we visually see the crude error in Table~\ref{tab:periods}. The value of
measurement 2 clearly deviates from the mean.

The quotient \(q\) of the masses of the balls can be calculated by inserting
values from Table~\ref{tab:constants} into equation~\eqref{eq:quotient}. The
result is \(q = 1.0038\ldots\).

\section{Error analysis}\label{sec:error-analysis}

If on this course you are calculating the value of some quantity \(q\) with a
formula, its error estimate \(\Delta q\) is done with the so-called
\emph{maximum error formula}
\begin{equation}\label{eq:max-err}
	\Delta q
	\leq
	\sum_i\maxerrterm{s}{x_i}\,.
\end{equation}
Here each of the quantities \(x_i\) in \eqref{eq:max-err} has either been
measured in the laboratory, or formed from the measurements via intermediary
results.~\cite[9]{analysing-errors} The partial errors that make up the maximum
error \eqref{eq:max-err} need to be tabulated in a manner similar to
Table~\ref{tab:max-err-terms}, and the quantity \(x_j\) that was the largest
source of error needs to be qualified based on this
table.~\cite[10]{analysing-errors}\cite[8]{malliselostus}

\begin{table}{
	An example of tabulating the partial errors \(\maxerrterm{q}{x_i}\). Here
	the number of quantities inserted into the formula for \(q\) is
	\(n\).
}{tab:max-err-terms}

	% The template class file defines math mode columns for tables. Inserting
	% capital L, C or R qualifies a column as a left-, center- or
	% right-aligned math mode column, where the math mode delimiters \( and \)
	% are not needed. If normal text is to be inserted in such a column, one
	% must use the \text{⋯} command.

	\begin{tabular}{ c | C C C | C }
		\toprule
		Term&\maxerrterm{s}{x_1} & \cdots & \maxerrterm{s}{x_n} & \Delta q \\
		\midrule
		Value (\(\squares{q}\)) & \cdots & \cdots & \cdots & \cdots\\
		\bottomrule
	\end{tabular}

\end{table}

In the case of regression analysis, one simply uses the readily provided
standard error of the regression method in question, which is usually the
method of least squares. Software like \emph{MS Excel} and \emph{LibreOffice
Calc} provide functions for performing this with a few mouse
clicks~\cite[12]{analysing-errors}, although more professional tools such as
Julia or Matlab might be used as well.


\section{Conclusion}\label{sec:conclusion}

The conculsion should directly respond to the questions posed in
section~\ref{sec:introduction}. The results formed in
section~\ref{sec:results} and their errors formed in
section~\ref{sec:error-analysis} need to be presented in rounded form. Your
own results should also be compared to values found in the literature, and/or
to a manufacturer's.

% We add the sources from lahteet.bib that we actually used via the \cite
% commands. We'll also add the title of this section to the table of contents.

\begingroup\raggedright%
\printbibliography[heading=bibintoc, title={References}]%
\endgroup

% Attach the original log book as an attachment and add the title of the
% section to the table of contents.

\def\liiteotsikko{Attachments}

\section*{\liiteotsikko}%
\addcontentsline{toc}{section}{\liiteotsikko}

% Use the reference names of the attachments as the items of this list.

\begin{itemize}
	\item[\ref{att:A}] The original log book
\end{itemize}


% The attachments begin. The command \appendix enumerates section titles with
% letters instead of Arabic numbers.

\appendix

% Include a PDF file wit hthe library pdfpages. Change the path below to point
% to your original log book.

\includepdf[%
	fitpaper,
	addtotoc = {
		1, % page number in attachment
		section, % title type
		1, % title level, 1 corresponds to a section
		The original log book, % Title of this attachment in the table of contents
		att:A % reference name in this document
	}
]{kuvat/mittaustulokset/times.pdf}

\end{document}
