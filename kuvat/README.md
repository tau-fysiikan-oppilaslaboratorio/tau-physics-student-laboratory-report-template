# Images

This folder contains the images placed in the figures of the report. Images
stored here need to be referenced from `main.tex` with

	kuvat/image.suffix ,

where `suffix` ∈ {`pdf`,`eps`,`png`,`jpg`}. In terms of LaTeΧ, the best suffix
to use is `pdf`.
