# TAU Physics student laboratory report template

This repository contains a report template for the reports that need to be
submitted in the Tampere University Physics student laboratory. You can
download the files to your computer by clicking on
<kbd>Download</kbd>/<kbd>⌊↓⌋</kbd> → <kbd>zip</kbd> at the top of this page
and saving the ZIP file to a folder of your choice.

The template has been written in the formal language [LaTeΧ], which is
designed for writing scientific (or just otherwise neat) documents. [Overleaf]
offers a short [LaTeΧ guide][Overleaf-LaTeX-opas] where the basics of the
language or covered, if they are not already familiar to you. Whereas writing
programs like MS Word and LibreOffice Writer follow the [WYSIWYG] principle,
LaTeΧ follows the [WYSIWYM] principle.

[LaTeΧ]: https://www.latex-project.org/
[Overleaf]: https://www.overleaf.com/
[Overleaf-LaTeX-opas]: https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes
[WYSIWYG]: https://en.wikipedia.org/wiki/WYSIWYG
[WYSIWYM]: https://en.wikipedia.org/wiki/WYSIWYM

## Table of Contents

* [Usage on Overleaf](#usage-on-overleaf-recommended)
* [Files and folders you are allowed to modify](#files-and-folders-you-are-allowed-to-modify)
* [Local use](#local-use)
* [Pre-defined commands and environments](#pre-defined-commands-and-environments)
* [License](#license)


## Usage on Overleaf (recommended)

[Overleaf] is a service that allows for collaborative writing of LaTeΧ
documents online, sort of like Microsoft Office 365 and Google Docs do.
This report template can be taken into use on Overleaf as follows.

Download the entire contents of this repository as a ZIP file by pressing
<kbd>Download</kbd> at the top of this page, and rename the downloaded ZIP
based on the report you are writing. [Create an Overleaf
account][Overleaf-tili] or simply [sign in to
Overleaf][Overleaf-kirjautuminen], if you already have an account.

When you have signed in to Overleaf, you can create a new writing project by
uploading the ZIP you downloaded from here by pressing <kbd>New Project</kbd>
→ <kbd>Upload Project</kbd> and choosing the downloaded ZIP file. Overleaf
will load the file and create a new project based on it. Finally, on the
created project page press <kbd>Menu</kbd> → <kbd>Compiler</kbd> →
<kbd>LuaLaTeX</kbd> to change the compiler used to compile the project. Once
all of this is done, you can start writing the report in the file `main.tex`
using the text editor of Overleaf. Don't forget to fill in the document
metadata in the file `docinfo.tex`.

You can download the PDF produced by Overleaf by pressing the <kbd>PDF</kbd>
button in the left sidebar on the Overleaf project page, or the <kbd>Download
PDF</kbd> button just above the compiled PDF view on the right side of the
project page. This PDF can then be uploaded to [Alva] for assessment by the
course staff.

[Overleaf-tili]: https://www.overleaf.com/register
[Overleaf-kirjautuminen]: https://www.overleaf.com/login


## Files and folders you are allowed to modify

Here is a short description of the files and folders the user of this template
is allowed to modify. Other than the `LICENSE` file, other components can be
modified as well, as long the the terms of the license are followed, if you
know what you are doing.

### [docinfo.tex](./docinfo.tex)

Contains the author and document metadata, given in the form of definitions

	\def\key{value}

Fill in the report metadata here, before turning it in to [Alva]. The names of
the `\keys` should be descriptive enough, so that you should be able to deduce
what the corresponding `values` need to be.

[Alva]: https://alva.tuni.fi/

### [main.tex](./main.tex)

This is the file the actual report is to be written in. The file contains text
that is meant to work as an example of how LaTeΧ is to be written.

### [lahteet.bib](./lahteet.bib)

Any references are to be added here. The file is written in the syntax of
[BibLaTeΧ][BibLaTeΧ file], a program that generates lists of references for
LaTeΧ documents. There are examples of online (`@online`) and literature
(`@book`) references in the file. To get a good grade, additional references
besides the course material will be needed.

All of the sources listed in this file are *not* automatically added to the
report. Only ones which are actually referenced via the command

	\cite{source key}

will be included in the list of references. The parameter `source key` is the
first key-less field in a source listing:

	@source type {
		source key,
		field 1 = {value},
		⋮
		field 𝑁 = {value}
	}

Different types of sources have different required fields. See [BibLaTeΧ
documentation][BibLaTeΧ doc], section 2.1 for details.

[BibLaTeΧ file]: https://www.overleaf.com/learn/latex/Bibliography_management_with_biblatex#The_bibliography_file
[BibLaTeΧ doc]: https://www.texlive.info/CTAN/macros/latex/contrib/biblatex/doc/biblatex.pdf

### [kuvat/](./kuvat)

This folder exists simply for the purpose of storing images related to a
report. These images are then referenced from the file `main.tex` via their
path `kuvat/name-of-image-file`.

### [preamble.tex](./preamble.tex)


This file is for declaring user-defined LaTeΧ commands, but this is by no
means necessary, as basic LaTeΧ and the commands provided by this template
will get you pretty far. The command definition library [xparse] is loaded by
the template class file, which simplifies the definition of more complex
commands. For example, an abbreviation for the math roman font command
`\mathrm` can be declared as follows:

	\NewDocumentCommand\mrm{m}{\mathrm{#1}}

Here `\mrm` is the name of our abbreviation command, the string `{m}` denotes
that our command takes one *mandatory* parameter, and the command definition
itself `{\mathrm{#1}}` says that this first mandatory parameter is to be
placed inside of a `\mathrm` invocation. Mor information on the command
definition syntax can be found in the [xparse documentation][xparse-doc].

[xparse]: https://ctan.org/pkg/xparse
[xparse-doc]: https://www.nic.funet.fi/pub/TeX/CTAN/macros/latex/contrib/l3packages/xparse.pdf


## Local use

You will need a text editor such as [VS Codium], a LaTeΧ distribution such as
[TeX Live] and a shell ([Zsh] | [Bash] | [Windows Terminal] | [PowerShell]).
Once all of these are installed, extract the zip archive you downloaded from
here to a folder of your choice and navigate to the folder with

	cd path/to/folder

You may then start editing the project with the text editor, just as you would
on Overleaf. To actually produce a PDF file, you must run

	lualatex main.tex && biber main && lualatex main.tex

to compile the document. Here `lualatex` is a LaTeΧ compiler and `biber` is a
program responsible for generating the list of references. Once the PDF is
produced, you can submit it to [Alva] for evaluation.

[VS Codium]: https://vscodium.com/
[TeX Live]: https://www.tug.org/texlive/
[Zsh]: https://www.zsh.org/
[Bash]: https://www.gnu.org/software/bash/
[Windows Terminal]: https://docs.microsoft.com/en-us/windows/terminal/install
[PowerShell]: https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell?view=powershell-7.2


## Pre-defined commands and environments

The template class file contains definitions for some useful commands and
environments. The *paired delimiter commands*

	\arcs{content}
	\set{content}
	\waves{content}
	\squares{content}
	\abs{content}

all work in a similar manner: they surround `content` with different kinds of
paired delimiters. For example `\squares{content}` produces `[content]` as its
output in the final PDF. The delimiters can be *scaled* to fit their contents
by prefixing the wavy brackets with a `*`. For example

	\arcs*{content}

scales the surrounding `(` and `)`, such that the `content` is not taller than
them. This is especially useful when typesetting tall fractions.

Also, the placement of figures and tables are done with the environments
`figure` and `table`. The syntax for figures is as follows:

	\begin{figure}[optional width setting]{caption text}{reference name}
		\includegrapics[width=\linewidth]{path/to/figure.pdf}
	\end{figure}

Tables are placed with the syntax

	\begin{table}[optional width setting]{caption text}{reference name}
		\begin{tabular}{column alignment settings for 𝑛 columns}
			cell (1,1) & cell (1,2) & ⋯ & cell (1,𝑛) \\
			cell (2,1) & cell (2,2) & ⋯ & cell (2,𝑛) \\
			⋮
			cell (𝑚,1) & cell (𝑚,2) & ⋯ & cell (𝑚,𝑛) \\
		\end{tabular}
	\end{table}

where `tabular` column alignment settings can be either `l`, `c` or `r`, for
*left*, *center* and *right*. If an entire column is to be typeset in math
mode, use `L`, `C` or `R` instead.

Figures and tables can be placed side-by-side by not leaving any blank space
between them in the source code:

	\begin{figure}[0.45\linewidth]{Caption for left figure.}{fig:left-figure}
		\includegrapics[width=\linewidth]{path/to/left/figure.pdf}
	\end{figure}
	\hfill
	\begin{figure}[0.45\linewidth]{Caption for right figure.}{fig:right-figure}
		\includegrapics[width=\linewidth]{path/to/right/figure.pdf}
	\end{figure}

If the resulting figures or tables end up being really small, this is
inadvisable.

There might be other commands defined in `tau-fys-oplab-kirjoituspohja.cls` as
well, such as `\pd` which is a shorthand for `\partial` (which typesets as a
∂)The ones listed above are the ones a user of this template should be aware
of.

## License

### TAU Physics student laboratory report template

Copyright © 2022 Santtu Söderholm

This work may be distributed and/or modified under the conditions of the LaTeX
Project Public License, either version 1.3 of this license or (at your option)
any later version. The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or later is part of
all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status "maintained". The Current Maintainer
of this work is Santtu Söderholm (<santtu.soderholm@tuni.fi>).

This work consists of the files in the folders `tex/` and
`kuvat/mittaustulokset/`.
