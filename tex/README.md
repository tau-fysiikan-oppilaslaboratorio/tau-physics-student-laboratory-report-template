# LaTeΧ-tiedostoja

Tämä kansio sisältää LaTeΧ-tiedostoja, joita ei ole tarkoitettu varsinaisesti
opiskelijoiden muokattavaksi, mutta jotka ladataan tiedoston `main.tex`
asettamassa yhteydessä. Vaikka tiedostoja ei ole tarkoitettu muokattavaksi,
saa niitä kuitenkin silmäillä, jos kiinnostaa.
