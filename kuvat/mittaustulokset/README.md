# Mittaustuloskuvia

Täällä ovat selostuspohjassa viitatut, kuvitteellisista mittaustuloksista
muodostetut kuvat, sekä niiden Julialla kirjoitettu muodostusskripti. Kuvat
voi muodostaa ajamalla komennon

	julia main.jl

tässä kansiossa. Tämä vaatii tietenkin [Julia-kääntäjän][Julia-kääntäjä]
asentamista. Tulostetut kuvat ovat PDF-formaatissa, sillä LaTeΧ-kääntäjät
ymmärtävät tässä muodossa olevia vektorigrafiikkakuvia parhaiten.

[Julia-kääntäjä]: https://julialang.org/downloads/
